## Описание
Проект включает в себя два репозитория:
- **Infra**(https://gitlab.com/goryamisery/Infra). В данном репозитории храним файлы для описания нашей инфраструктуры как код (IaC) для тестового(test_env) и продакшн(prod_env) окружений.
- **Service**(https://gitlab.com/goryamisery/service). Репозиторий для CI/CD нашего приложения, так же у gitlab.com имеется Container Registry.

Платформа виртуализации: 
- VMware vCloud Director (https://vcloud.mts.by/),

Инструменты IaC:
- terraform (https://developer.hashicorp.com/terraform/docs),
- ansible (https://docs.ansible.com/ansible/latest/user_guide/index.html).

Для начала работ обязательно ознакомьтесь и выполните шаги из документациии по быстрому старту:
- VMware vCloud Director (https://docs.vmware.com/en/VMware-Cloud-Director/index.html),
- Ansible (https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- Terraform (https://developer.hashicorp.com/terraform/docs)
___

## Настройка среды для приложения
1. Добавляем А-запись на DNS сервере домена (в проекте используется `http://antonovich.space`)
2. Перед деплоем тестового или продакшн-окружения клонируем репозиторий Infra:
`git clone https://gitlab.com/goryamisery/Infra.git`
3. Захододим в требуемое окружение и указываем в `variables.tf` credentials от тенанта vCloud Director. В `main.tf` описана наша инфраструктура
4. Плейбуки ansible для окружений:
    - ssh-config.yml (установка сгенерированного public-key на все удаленные хосты, настройка безопасной аутентификации ко всем сервисам)
    - deploy-monitoring.yaml (установка всех сервисов для мониторинга)
    - nginx-for-lb.yaml (настройка балансировщика нагрузки для приложения)
___
## Развертывание инфраструктуры
1. Для развертывания инфраструктуры перейдите в одно из окружений папке Infra
2. Далее по аналогии  инициализируем `terraform init` и для начала установки  `terraform apply --auto-approve`
3. После окончания развертывания инфраструктуры, выполняем установку всех зависимостей через `ansible-playbook`
___
## Дерево проекта
- **Infra** (деплой окружения)
  - test_env 
  - prod_env
    
___
- **Service** (платформа для CI/CD)
  - app(развертывание приложения в одном из окружений)
___

## Схема проекта
- [_SchemeProject_](https://gitlab.com/goryamisery/Infra/-/blob/main/docs/scheme.pdf)

