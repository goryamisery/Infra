########Credentials###########
provider "vcd" {
  user                 = var.vcd_user
  password             = var.vcd_pass
  org                  = var.vcd_org
  vdc                  = var.vcd_vdc
  url                  = var.vcd_url
  allow_unverified_ssl = var.vcd_allow_unverified_ssl
}

data "vcd_resource_list" "edge_gateway_name" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "edge_gateway_name"
  resource_type = "vcd_edgegateway" # find gateway name
  list_mode     = "name"
}

data "vcd_edgegateway" "gateway" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name         = element(data.vcd_resource_list.edge_gateway_name.list,1)
}

data "vcd_network_routed_v2" "network-org" {
  org = var.vcd_org
  name            = "org-1"
  edge_gateway_id = data.vcd_edgegateway.gateway.id
}

######VMs for LB###########

resource "vcd_vm" "forLB1" { 
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "nginxLB1"
  vapp_template_id = "8fffc724-97ce-4b82-a55b-ca66ec70f7b7"
  memory        = 1024
  cpus          = 1
  cpu_cores     = 1
  
  network {
    type               = "org"
    name               = "org-1"
    ip_allocation_mode = "POOL"
    is_primary         = true
    connected          = true
  }
  power_on = true
}

resource "vcd_vm" "forLB2" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "nginxLB2"
  vapp_template_id = "8fffc724-97ce-4b82-a55b-ca66ec70f7b7"
  memory        = 1024
  cpus          = 1
  cpu_cores     = 1

  network {
    type               = "org"
    name               = "org-1"
    ip_allocation_mode = "POOL"
    is_primary         = true
    connected          = true
  }
  power_on = true
}

resource "vcd_vm" "Application" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "Application"
  vapp_template_id = "8fffc724-97ce-4b82-a55b-ca66ec70f7b7"
  memory        = 1024
  cpus          = 1
  cpu_cores     = 1

  network {
    type               = "org"
    name               = "org-1"
    ip_allocation_mode = "POOL"
    is_primary         = true
    connected          = true
  }
  power_on = true
}

#####LoadBalancer##########

resource "vcd_lb_virtual_server" "http" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  edge_gateway = var.edge_gateway

  name       = "my-virtual-server"
  ip_address = "134.17.89.221"
  protocol   = "http"
  port       = 80

  app_profile_id = vcd_lb_app_profile.http.id
  server_pool_id = vcd_lb_server_pool.web-servers.id
#  app_rule_ids   = [vcd_lb_app_rule.redirect.id]

}

resource "vcd_lb_service_monitor" "monitor" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  edge_gateway = var.edge_gateway
  name        = "default_http_monitor"
  interval    = "5"
  timeout     = "15"
  max_retries = "3"
  type        = var.protocol
  method      = "GET"
  url         = "/health"
  send        = "{\"key\": \"value\"}"
  extension = {
    content-type = "application/json"
    linespan     = ""
  }
}

resource "vcd_lb_server_pool" "web-servers" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  edge_gateway = var.edge_gateway

  name                 = "web-servers"
  description          = "description"
  algorithm            = "httpheader"
  algorithm_parameters = "headerName=host"
  enable_transparency  = true

  monitor_id = vcd_lb_service_monitor.monitor.id

  member {
    condition       = "enabled"
    name            = "nginx-1"
    ip_address      = "10.10.90.70"
    port            = 80
    monitor_port    = 80
    weight          = 1
    min_connections = 0
    max_connections = 100
  }

  member {
    condition       = "drain"
    name            = "nginx-2"
    ip_address      = "10.10.90.71"
    port            = 80
    monitor_port    = 80
    weight          = 2
    min_connections = 1
    max_connections = 99
  }
}

resource "vcd_lb_app_profile" "http" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  edge_gateway = var.edge_gateway
  name         = "http-app-profile"
  type         = var.protocol

}

