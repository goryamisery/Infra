####Credentials
provider "vcd" {
  user                 = var.vcd_user
  password             = var.vcd_pass
  org                  = var.vcd_org
  vdc                  = var.vcd_vdc
  url                  = var.vcd_url
  allow_unverified_ssl = var.vcd_allow_unverified_ssl
}

data "vcd_resource_list" "edge_gateway_name" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "edge_gateway_name"
  resource_type = "vcd_edgegateway" # find gateway name
  list_mode     = "name"
}

data "vcd_edgegateway" "gateway" {
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name         = element(data.vcd_resource_list.edge_gateway_name.list,1)
}

data "vcd_network_routed_v2" "network-org" {
  org = var.vcd_org
  name            = "org-1"
  edge_gateway_id = data.vcd_edgegateway.gateway.id
}

####VM monitoring

resource "vcd_vm" "monitoring" { 
  org          = var.vcd_org
  vdc          = var.vcd_vdc
  name          = "service-monitoring"
  vapp_template_id = "8fffc724-97ce-4b82-a55b-ca66ec70f7b7"
  memory        = 4096
  cpus          = 2
  cpu_cores     = 1
  
  network {
    type               = "org"
    name               = "org-1"
    ip_allocation_mode = "MANUAL"
    ip                 = "10.10.90.33"
    is_primary         = true
    connected          = true
  }
  power_on = true
}

