####Credential########
variable "vcd_user" {
    type = string
    default = "user"
}

variable "vcd_pass" {
    type = string
    
}

variable "vcd_org"{
    type = string
    default = "test_gantonovich"
}

variable "vcd_vdc" {
    type = string
    default = "test_gantonovich" 
}

variable "vcd_url" {
    type = string
    default = "https://vcloud.mts.by/api"
}

variable "vcd_allow_unverified_ssl" {
    type = string
    default = true
}

